import { render, screen } from "@testing-library/react";
import App from "./App";
import React from "react";
import { createStore } from "redux";
import counterReducer from "./Components/reducers/counter";
import allReducers from "./Components/reducers/index";
import { Provider } from "react-redux";

const store = createStore(
  counterReducer,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
test("renders learn react link", () => {
  render(
    <Provider store={store}>
      <App />
    </Provider>
  );
});
