import { Increment } from "../actions";

describe("actions", () => {
  it("should create an action to add a todo", () => {
    const v = 5;
    const expectedAction = {
      type: "INCREMENT",
      payload: v,
    };
    expect(Increment(5)).toEqual(expectedAction);
  });
});
