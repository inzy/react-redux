export const IncrementAsync = (v) => {
  return {
    type: "INCREMENT",
    payload: v,
  };
};

export const Increment = (v) => {
  return (dispatch) => {
    setTimeout(() => dispatch(IncrementAsync(v)), 1000);
  };
};
export const Decrement = (v) => {
  return (dispatch) => {
    setTimeout(() => dispatch(DecrementAsync(v)), 1000);
  };
};

export const DecrementAsync = () => {
  return {
    type: "DECREMENT",
  };
};

export const Logged = () => {
  return {
    type: "SIGN_IN",
  };
};
