import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { Increment, Decrement } from "../actions/actions";

class CounterFile extends React.Component {
  render() {
    return (
      <div className="App">
        <label>label</label>
        <div
          data-testid="mydiv"
          className="counterClass"
          style={this.props.counter % 2 ? { color: "red" } : { color: "green" }}
        >
          counter:{this.props.counter}
        </div>
        <input id="search" type="text" defaultValue="" />
        <h1>inzamam</h1>
        <button data-testid="myIButton" onClick={() => this.props.Increment(1)}>
          +
        </button>
        <button data-testid="myDButton" onClick={() => this.props.Decrement(1)}>
          -
        </button>

        {/* <div>isLogged : {this.props.isLogged ? "12312" : "asdasd"}</div> */}
      </div>
    );
  }
}

function mapStateToProps(state) {
  console.log("state...", state);
  return {
    counter: state.counter,
  };
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(
    {
      Increment: Increment,
      Decrement: Decrement,
    },
    dispatch
  );
}
export default connect(mapStateToProps, mapDispatchToProps)(CounterFile);
