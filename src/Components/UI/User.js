import React, { useState, useEffect } from "react";

function getUser() {
  return Promise.resolve({ id: "1", name: "Robin" });
}

function User() {
  const [search, setSearch] = useState("");
  const [user, setUser] = useState(null);

  useEffect(() => {
    const loadUser = async () => {
      const user = await getUser();
      setUser(user);
    };

    loadUser();
  }, []);

  function handleChange(event) {
    setSearch(event.target.value);
  }

  return (
    <div>
      {user ? <p>Signed in as {user.name}</p> : null}
      <input id="search" type="text" value="qw" onChange={handleChange} />
      <p>Searches for {search ? search : "..."}</p>
      <h1>inzamam</h1>
    </div>
  );
}

export default User;
