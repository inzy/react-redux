import React from "react";
import { render, screen, fireEvent, cleanup } from "@testing-library/react";
import userEvent from "@testing-library/user-event";
import User from "../User";
afterEach(cleanup);
// describe("User", () => {
//   test("renders User component", async () => {
//     render(<User />);

//     expect(screen.queryByText(/Signed in as/)).toBeNull();
//     expect(await screen.findByText(/Signed in as/)).toBeInTheDocument();
//   });
// });

// describe("UserUser", () => {
//   test("renders User component", async () => {
//     render(<User />);
//     await screen.findByText(/Signed in as/);
//     screen.debug();

//     fireEvent.change(screen.getByRole("textbox"), {
//       target: { type: "number", value: "mynewValue" },
//     });

//     screen.debug();
//   });
// });

describe("User", () => {
  test("renders User component", async () => {
    const { getByRole } = render(<User />);
    const input = getByRole("textbox");
    // wait for the user to resolve
    await screen.findByText(/Signed in as/);

    expect(screen.queryByText(/Searches for JavaScript/)).toBeNull();

    await userEvent.type(input, "JavaScrip");

    expect(screen.getByText(/Searches for /)).toBeInTheDocument();
    // expect(input.value).toBe("01:01");
  });
});
