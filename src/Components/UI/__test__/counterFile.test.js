import React, { isValidElement } from "react";
import ReactDom from "react-dom";
import CounterFile from "../counterFile";
import counterReducer from "../../reducers/counter";
import thunk from "redux-thunk";
import { createStore, applyMiddleware } from "redux";
import userEvent from "@testing-library/user-event";
import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import "@testing-library/jest-dom";
import allReducers from "../../reducers/index";
import { Provider } from "react-redux";
import { Increment } from "../../actions/actions";

// it("renders without crashing", () => {
//   const div = document.createElement("div");
//   ReactDom.render(
//     <Provider store={store}>
//       <CounterFile />
//     </Provider>,
//     div
//   );
//   ReactDom.unmountComponentAtNode(div);
// });
// describe("CounterFile", () => {
//   test("renders CounterFile component", () => {
//     render(
//       <Provider store={store}>
//         <CounterFile />
//       </Provider>
//     );

//     expect(screen.getByLabelText("label"));
//   });
// });

// it("insert text in h1", () => {
//   render(
//     <Provider store={store}>
//       <CounterFile />
//     </Provider>
//   );
//   expect(screen.queryByRole("textbox")).toBeInTheDocument();
// });

// it("insert text in h1", () => {
//   render(
//     <Provider store={store}>
//       <CounterFile />
//     </Provider>
//   );
//   // userEvent.type(screen.getByLabelText(/Raw Phone/i), "1234567890");
//   userEvent.type(screen.getByLabelText(/Raw Phone/i), "1234567890");
// });

// it("renders", () => {
//   const { asFragment } = render(
//     <Provider store={store}>
//       <CounterFile />
//     </Provider>
//   );
//   expect(asFragment()).toMatchSnapshot();
// });

// describe("CounterFile", () => {
//   test("renders CounterFile component", async () => {
//     render(
//       <Provider store={store}>
//         <CounterFile />
//       </Provider>
//     );

//     fireEvent.click(screen.getByText("+"));
//     // await screen.findByText(/counter:/);
//     screen.debug();
//   });
// });

const renderComponent = ({ counter }) =>
  render(
    <Provider
      store={createStore(counterReducer, { counter }, applyMiddleware(thunk))}
    >
      <CounterFile />
    </Provider>
  );
const renderComponent1 = ({
  store = createStore(counterReducer, applyMiddleware(thunk)),
} = {}) => {
  return {
    ...render(
      <Provider store={store}>
        <CounterFile />
      </Provider>
    ),
  };
};

function isEven(v) {
  console.log("value...", v);

  return v % 2;
}

// it("renders initial count", async () => {
//   // Render new instance in every test to prevent leaking state
//   const { getByText } = renderComponent({ counter: 5 });

//   await screen.findByText(/counter:5/);
// });

//////////////////Increment////////////////////
// it("increments count", async () => {
//   // Render new instance in every test to prevent leaking state
//   const { getByTestId, getByText } = renderComponent({ counter: 4 });
//   expect(getByTestId("mydiv")).toHaveTextContent("counter:4");
//   expect(getByTestId("mydiv")).toHaveStyle("color: green");
//   // const value = isEven({ counter });
//   // const button = getByText("");

//   fireEvent.click(getByText("+"));
//   await screen.findByText("counter:4");
//   // expect(await screen.getByTestId("mydiv")).toHaveTextContent("counter:6");

//   // expect(button).toHaveTextContent(/counter:5/);
// });

// it("Decrement count", async () => {
//   // Render new instance in every test to prevent leaking state
//   const { getByTestId, getByText, findByText } = renderComponent({
//     counter: 4,
//   });
//   expect(getByTestId("mydiv")).toHaveTextContent("counter:4");
//   expect(getByTestId("mydiv")).toHaveStyle("color: green");
//   // const value = isEven({ counter });
//   // const button = getByText("");

//   fireEvent.click(getByText("-"));

//   await findByText("counter:4");
//   expect(getByTestId("mydiv")).toHaveTextContent("counter:4");

//   // expect(button).toHaveTextContent(/counter:5/);
// });

it("custom Store", async () => {
  const counter = 4;
  const store = createStore(
    counterReducer,
    { counter },
    applyMiddleware(thunk)
  );
  const { getByTestId, findByTestId } = renderComponent1({ store });
  const add = getByTestId("myIButton");

  add.click();
  const count = await findByTestId("mydiv");

  // expect(count.innerHTML).toBe("0");
});
