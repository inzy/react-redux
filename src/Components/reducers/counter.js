const Counter = (counter = 1, action) => {
  switch (action.type) {
    case "INCREMENT":
      return counter + action.payload;

    case "DECREMENT":
      return counter - action.payload;

    default:
      return counter;
  }
};

export default Counter;
