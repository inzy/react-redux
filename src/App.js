import "./App.css";
import CounterFile from "./Components/UI/counterFile";
function App() {
  return (
    <div className="App">
      <CounterFile />
    </div>
  );
}

export default App;
